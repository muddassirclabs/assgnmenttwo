/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.app.entities;

import static com.app.util.AppConstants.MAX_SIZE;
import java.util.Random;
import static com.app.main.Main.list;

/**
 *
 * @author Muddassir Iqbal
 */
public class Producer extends Thread {

    private volatile boolean producerRunning = true; // to keep record when to turn down the thread
    private static Random generator = new Random(); // to generate random number

    // This is the overriden run() method
    @Override
    public void run() {
        while (producerRunning) {
            addDataToList();
        }
    }

    // this will set flag to false so as to shutdown thread
    public void shutdownProducer() {
        producerRunning = false;
    }

    // This method will try to add data to list if list is not full
    private void addDataToList() {
        synchronized (list) { //list - is the mutex object
            while (list.size() >= MAX_SIZE) { // check for full list
                try {
                    System.out.println("Producer Waiting.");
                    list.wait();   // will wait until list is not full
                } catch (InterruptedException ex) {
                    System.out.println("Exception :" + ex);
                }
            }
            list.add(generator.nextInt(100)); // adding item to list
            System.out.println("Added: " + list.get(list.size() - 1));
            list.notifyAll(); // notify all consumers to contibue processing            
        }
    }
}
