/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.app.entities;

import static com.app.main.Main.list;

/**
 *
 * @author Muddassir Iqbal
 */
public class Consumer extends Thread {

    private volatile boolean consumerRunning = true; // to keep record when to turn down the thread

    // This is the overriden run() method
    @Override
    public void run() {
        while (consumerRunning) {
            removeDataFromList();
        }
    }

    // this will set flag to false so as to shutdown thread
    public void shutdownConsumer() {
        consumerRunning = false;
    }

    // This method will try to remove an item from list if list is not empty
    private void removeDataFromList() {
        /*
         * Note - 1. Mutex lock must be kept on common object
         *        2. notify(), notifyAll() and wait() must be on mutex object
         *        3. synchronized (Consumer.this) [This is Class level Lock]
         *        4. synchronized (consumerObject) [This is Object level lock]
         */

        synchronized (list) {
            while (list.isEmpty()) {
                try {
                    System.out.println("Consumer Waiting.");
                    list.wait();
                } catch (InterruptedException ex) {
                    System.out.println("Exception :" + ex);
                }
            }
            System.out.println("Removed: " + list.remove(0)); // remove the first item
            list.notify(); // Notify the producer to add item

        }
    }
}
