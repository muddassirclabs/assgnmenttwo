/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.app.main;

import com.app.entities.Consumer;
import com.app.entities.Producer;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Muddassir Iqbal
 */
public class Main {

    // This will act as common buffer for both Producer and Consumer
    public static List<Integer> list = new ArrayList<>();

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        // This will automatically create one thread for each object
        Producer producer = new Producer();
        Consumer consumer = new Consumer();

        // Setting Thread priorities
        producer.setPriority(Thread.MAX_PRIORITY);
        consumer.setPriority(Thread.MIN_PRIORITY);

        // These will invoke the threads
        producer.start();
        consumer.start();

        System.out.println("Press return to stop process...");

        Scanner scan = new Scanner(System.in);
        scan.nextLine();

        // These will attempt to stop the threads
        consumer.shutdownConsumer();
        producer.shutdownProducer();

        System.out.println("Producer and Consumer Stopped.");
        System.exit(0);
    }
}
